package critic

import (
	"fmt"
	"strings"

	"github.com/expr-lang/expr"
	"github.com/expr-lang/expr/vm"
)

type Token struct {
	Value string
	Score int
}

type Classification struct {
	Description string
	Score       int
	Object      any
}

type Classifications []Classification

func (cs Classifications) Score() (score int) {
	for _, classification := range cs {
		score += classification.Score
	}
	return
}

type ClassificationRule struct {
	Description string
	Expr        *vm.Program
}

type Classifier struct {
	time   Time
	rules  []ClassificationRule
	Tokens map[string]int
}

func (c Classifier) Classify(classified Classified) (Classifications, error) {
	env := DefaultEnv()
	env["user"] = classified.User
	env["item"] = classified.Item
	env["time"] = c.time

	classifications := Classifications{}
	for _, classifier := range c.rules {
		output, err := expr.Run(classifier.Expr, env)
		if err != nil {
			return nil, fmt.Errorf("could not evaluate expression for '%s': %w", classifier.Description, err)
		}

		switch o := output.(type) {
		case Classification:
			if o.Score > 0 {
				classifications = append(classifications, o)
			}
		case Classifications:
			for _, c := range o {
				if c.Score != 0 {
					classifications = append(classifications, c)
				}
			}
		case int:
			if o != 0 {
				classifications = append(classifications, Classification{
					Description: classifier.Description,
					Score:       o,
				})
			}
		default:
			return nil, fmt.Errorf("Unsupported return type %T for '%s'", output, classifier.Description)
		}
	}
	return classifications, nil
}

func (c *Classifier) AddRule(expression, description string) error {
	program, err := c.compileExpression(expression)
	if err != nil {
		return err
	}

	c.rules = append(c.rules, ClassificationRule{
		Description: description,
		Expr:        program,
	})

	return nil
}

func (c Classifier) compileExpression(expression string) (*vm.Program, error) {
	env := DefaultEnv()
	program, err := expr.Compile(expression,
		expr.Env(env),
		expr.Function(
			"C",
			func(params ...any) (any, error) {
				predicate := params[0].(bool)
				score := params[1].(int)
				description := params[2].(string)

				if predicate {
					return Classification{
						Description: description,
						Score:       score,
					}, nil
				} else {
					return Classification{
						Description: description,
						Score:       0,
					}, nil
				}
			},
			new(func(bool, int, string) Classification),
		),
		expr.Function(
			"CS",
			func(params ...any) (any, error) {
				classifications := params[0].([]Classification)

				return Classifications(classifications), nil
			},
			new(func([]Classification) Classifications),
		),
		expr.Function(
			"flatten",
			func(params ...interface{}) (interface{}, error) {
				nestedClassifications, ok := params[0].([]any)
				if !ok {
					return nil, fmt.Errorf("flatten: unexpected type for argument 1: %T", params[0])
				}

				classifications := Classifications{}
				for _, innerClassifications := range nestedClassifications {
					classifications = append(classifications, innerClassifications.(Classifications)...)
				}

				return classifications, nil
			},
			new(func([]Classifications) Classifications),
		),
		expr.Function(
			"SumList",
			func(params ...any) (any, error) {
				lst := []int{}
				for _, e := range params[0].([]any) {
					lst = append(lst, e.(int))
				}

				return SumList(lst), nil
			},
			new(func(lst []int) (any, error)),
		),
		expr.Function(
			"classifyText",
			func(params ...any) (any, error) {
				text := params[0].(string)
				object := params[1]
				return c.classifyText(text, object), nil
			},
			c.classifyText,
		),
	)

	return program, err
}

func (c Classifier) classifyText(text string, object any) (cs Classifications) {
	textTokens := strings.Fields(text)

	for _, token := range textTokens {
		tokenTrimmed := strings.Trim(token, "!?,.()[]{}-_=+;:'\"@#$%^&*/\\|`~")
		tokenLower := strings.ToLower(tokenTrimmed)
		if score, ok := c.Tokens[tokenLower]; ok {
			cs = append(cs, Classification{
				Description: fmt.Sprintf("contains '%s'", tokenLower),
				Score:       score,
				Object:      object,
			})
		}
	}

	return
}
