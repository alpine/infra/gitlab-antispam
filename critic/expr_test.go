package critic

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestDiffDays(t *testing.T) {
	at := Time{
		Now: time.Date(2022, 6, 24, 12, 0, 0, 0, time.UTC),
	}

	days := at.DiffDays(time.Date(2022, 6, 20, 12, 0, 0, 0, time.UTC))

	assert.Equal(t, 4, days)
}
