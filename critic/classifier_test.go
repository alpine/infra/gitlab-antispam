package critic

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestClassifySpamWithoutClassifiers(t *testing.T) {
	classified := Classified{}

	classifictions, err := Classifier{}.Classify(classified)
	require.NoError(t, err)

	assert.Len(t, classifictions, 0)
}

func TestClassifyCanAccessClassifiedObject(t *testing.T) {
	classified := Classified{
		User: User{
			Username: "spam",
		},
	}
	classifier := Classifier{}

	mustCompile(t, `user.Username == "spam" ? 5 : 0`, "Username is spam", &classifier)

	classifications, err := classifier.Classify(classified)
	require.NoError(t, err)
	require.Len(t, classifications, 1, "expected 'Username is spam' rule to match")
}

func TestClassifyHandlesMultipleRules(t *testing.T) {
	classified := Classified{
		User: User{
			Username:  "spam",
			CreatedAt: time.Date(2022, 4, 20, 15, 14, 1, 0, time.UTC),
		},
		Item: Item{
			Title:       "This is absolutely not spam",
			Description: "Please buy my product so that I can get money",
		},
	}

	classifier := Classifier{time: Time{Now: time.Date(2022, 6, 24, 12, 0, 0, 0, time.UTC)}}

	mustCompile(t, `user.Username == "spam" ? 5 : 0`, "Username is spam", &classifier)
	mustCompile(t, `time.DiffDays(user.CreatedAt) < 14 ? 5 : 0`, "User was created less then 2 weeks ago", &classifier)

	classifications, err := classifier.Classify(classified)

	require.NoError(t, err)
	assert.Len(t, classifications, 1, "expected only one rule to match")
}

func TestClassifyDateDiff(t *testing.T) {
	classified := Classified{
		User: User{
			Username:  "spam",
			CreatedAt: time.Date(2022, 6, 20, 15, 14, 1, 0, time.UTC),
		},
	}

	classifier := Classifier{time: Time{Now: time.Date(2022, 6, 24, 12, 0, 0, 0, time.UTC)}}

	mustCompile(t, `time.DiffDays(user.CreatedAt) < 14 ? 5 : 0`, "User was created less then 2 weeks ago", &classifier)

	classifications, err := classifier.Classify(classified)
	require.NoError(t, err)
	assert.Len(t, classifications, 1)
}

func TestClassifyText(t *testing.T) {
	classified := Classified{
		Item: Item{
			Title: "this is a spam package, please do not remove",
		},
	}

	classifier := Classifier{
		Tokens: map[string]int{
			"spam":    5,
			"package": -1,
		},
	}

	mustCompile(t, `classifyText(item.Title, item)`, "Title does not contain spam", &classifier)

	classifications, err := classifier.Classify(classified)
	require.NoError(t, err)
	assert.Len(t, classifications, 2)
	assert.Equal(t, 4, classifications.Score())
}

func TestClassifyFunction(t *testing.T) {
	classified := Classified{
		Item: Item{
			Title:       "This is a test spam message",
			Description: "Nothing good comes from that",
		},
	}

	classifier := Classifier{}

	mustCompile(t, `C(item.Title contains "spam", 6, "item title contains spam")`, "spam is naughty'", &classifier)

	classifications, err := classifier.Classify(classified)
	require.NoError(t, err)
	require.Len(t, classifications, 1)

	assert.Equal(t, "item title contains spam", classifications[0].Description)
	assert.Equal(t, 6, classifications[0].Score)
}

func TestClassifyNested(t *testing.T) {
	classified := Classified{
		Item: Item{
			Title:       "This is a test spam message",
			Description: "I have a business proposal",
		},
	}

	classifier := Classifier{
		Tokens: map[string]int{
			"business": 5,
			"proposal": 3,
		},
	}

	mustCompile(t, `classifyText(item.Description, item)`, "description contains spam", &classifier)

	classifications, err := classifier.Classify(classified)
	require.NoError(t, err)
	require.Len(t, classifications, 2)

	assert.Equal(t, "contains 'business'", classifications[0].Description)
	assert.Equal(t, "contains 'proposal'", classifications[1].Description)
	assert.Equal(t, 8, classifications.Score())
}

func mustCompile(t *testing.T, expression string, description string, classifier *Classifier) {
	t.Helper()

	err := classifier.AddRule(expression, description)
	require.NoError(t, err, "could not compile expression for: %s", description)

}
