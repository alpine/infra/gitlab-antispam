package critic

import "time"

type Item struct {
	Title       string
	Description string
	Locale      Locale
}

type User struct {
	Username         string
	ID               int
	Email            string
	Bio              string
	CreatedAt        time.Time
	ConfirmedAt      time.Time
	LastSignInAt     time.Time
	Identities       []UserIdentity
	UserAssociations UserAssociations
}

type UserIdentity struct {
	Provider    string
	ExternalUID string
}

type UserAssociations struct {
	GroupsCount        int
	ProjectsCount      int
	IssuesCount        int
	MergeRequestsCount int
}

type Classified struct {
	User User
	Item Item
}

type Locale struct {
	Language   string
	Script     string
	Confidence float64
}

func DefaultEnv() map[string]any {
	return map[string]any{
		"user": User{},
		"item": Item{},
		"time": Time{},
	}
}
