package critic

import (
	"math"
	"time"
)

type Time struct {
	Now time.Time
}

func (at Time) DiffDays(t time.Time) int {
	if at.Now.IsZero() {
		at.Now = time.Now()
	}

	diff := at.Now.Sub(t)

	return int(math.Ceil(float64(diff.Nanoseconds()) / (float64(time.Hour) * 24)))
}

func SumList(lst []int) (sum int) {
	for _, e := range lst {
		sum += e
	}
	return
}
