FROM alpinelinux/golang:latest AS builder

COPY --chown=build:build . /home/build/gitlab-antispam

WORKDIR /home/build/gitlab-antispam

RUN --mount=type=cache,target=/home/build/go,uid=1000,gid=1000 \
    --mount=type=cache,target=/home/build/.cache/go-build,uid=1000,gid=1000 \
    go build -v -o gitlab-spamcheck ./spamcheck/cmd

FROM alpine:edge

RUN <<EOF
    adduser -D spam
    install -dm0755 -ospam -gspam /home/spam/events
    mkdir -p /etc/spamcheck
EOF

COPY --from=builder \
    /home/build/gitlab-antispam/gitlab-spamcheck \
    /usr/local/bin/gitlab-spamcheck

USER spam
CMD ["/usr/local/bin/gitlab-spamcheck", "--config", "/etc/spamcheck/config.yaml"]
EXPOSE 8001
