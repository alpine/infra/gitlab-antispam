package main

import (
	"fmt"
	"io"
	"net"
	"os"

	"github.com/spf13/pflag"
	"gitlab.alpinelinux.org/alpine/infra/gitlab-antispam/critic"
	"gitlab.alpinelinux.org/alpine/infra/gitlab-antispam/spamcheck"
	"golang.org/x/exp/slog"
	"google.golang.org/grpc"
	"gopkg.in/yaml.v3"
)

type Tokens map[string]int

type Rule struct {
	Expression  string
	Description string
}

type Config struct {
	Tokens                  Tokens  `yaml:"tokens"`
	Rules                   []Rule  `yaml:"rules"`
	ScoreThreshold          float32 `yaml:"score_threshold"`
	ClassificationThreshold int     `yaml:"classification_threshold"`
}

func main() {
	port := pflag.Uint("port", 8001, "The port to listen on")
	configPath := pflag.String("config", "config.yaml", "Path the config file")
	eventPath := pflag.String("events", "", "Write events to the specified file")

	pflag.Parse()

	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{Level: slog.LevelInfo})))

	listener, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", *port))

	if err != nil {
		panic(err)
	}

	opts := []grpc.ServerOption{}
	grpcServer := grpc.NewServer(opts...)

	classifier, config, err := loadClassifier(*configPath)

	if err != nil {
		panic(err)
	}

	var eventsOut io.Writer

	if *eventPath != "" {
		eventsOut, err = os.OpenFile(*eventPath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			panic(err)
		}
	}

	spamcheck.RegisterHealthServer(grpcServer, spamcheck.HealthCheckService{})
	spamcheck.RegisterSpamcheckServiceServer(
		grpcServer,
		spamcheck.NewSpamCheckService(
			classifier,
			&spamcheck.SpamCheckServiceOptions{
				ClassificationThreshold: config.ClassificationThreshold,
				ScoreThreshold:          config.ScoreThreshold,
				EventWriter:             eventsOut,
			},
		),
	)

	slog.Info("Starting grpc server", "port", "8001")
	grpcServer.Serve(listener)
}

func loadClassifier(filename string) (*critic.Classifier, *Config, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, nil, err
	}

	config := Config{}
	decoder := yaml.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		return nil, nil, err
	}

	classifier := &critic.Classifier{}
	classifier.Tokens = config.Tokens

	for _, rule := range config.Rules {
		err = classifier.AddRule(rule.Expression, rule.Description)
		if err != nil {
			return nil, nil, err
		}
	}

	return classifier, &config, nil
}
