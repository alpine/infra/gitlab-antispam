package spamcheck

import (
	"context"
)

type HealthCheckService struct {
}

var _ HealthServer = (*HealthCheckService)(nil)

func (hcs HealthCheckService) Check(ctx context.Context, r *HealthCheckRequest) (*HealthCheckResponse, error) {
	resp := &HealthCheckResponse{
		Status: HealthCheckResponse_SERVING,
	}
	return resp, nil
}

func (hcs HealthCheckService) Watch(*HealthCheckRequest, Health_WatchServer) error {
	return nil
}

func (hcs HealthCheckService) mustEmbedUnimplementedHealthServer() {

}
