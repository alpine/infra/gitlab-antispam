package spamcheck

import (
	"bytes"
	"context"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.alpinelinux.org/alpine/infra/gitlab-antispam/critic"
	"golang.org/x/exp/slog"
)

func init() {
	slog.SetDefault(slog.New(slog.NewTextHandler(io.Discard, nil)))
}

func TestScore(t *testing.T) {
	assert := assert.New(t)

	assert.InDelta(float32(0), logistic(0, 20), 0.0001, "classification of 0 should score 0.0")
	assert.InDelta(float32(0.5), logistic(20, 20), 0.0001, "classification of 20 should score 0.5")
	assert.InDelta(float32(1), logistic(40, 20), 0.0001, "classification of 40 should score 1")
}

func TestSpamcheckClassifiesSpamIssue(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	classifier := critic.Classifier{
		Tokens: map[string]int{
			"spam": 10,
		},
	}

	require.NoError(classifier.AddRule(`classifyText(item.Title, item)`, "spam title"))
	require.NoError(classifier.AddRule(`classifyText(item.Description, item)`, "spam description"))

	spamcheckService := NewSpamCheckService(&classifier, &SpamCheckServiceOptions{ScoreThreshold: 20})
	verdict, err := spamcheckService.CheckForSpamIssue(context.Background(), &Issue{
		Title:       "This is a spam issue",
		Description: "It should have a non-zero score because it contains spam",
		User:        &User{Username: "spammer"},
		Project:     &Project{ProjectId: 1, ProjectPath: "test/project"},
	})

	require.NoError(err)
	assert.InDelta(0.5, verdict.Score, 0.001)
}

func TestSpamcheckWritesEvent(t *testing.T) {
	assert := assert.New(t)

	buf := bytes.Buffer{}

	classifier := critic.Classifier{}
	spamCheckService := NewSpamCheckService(&classifier, &SpamCheckServiceOptions{
		EventWriter: &buf,
	})

	spamCheckService.CheckForSpamIssue(context.Background(), &Issue{
		Title:       "",
		Description: "",
		User:        &User{Username: ""},
		Project:     &Project{ProjectId: 1, ProjectPath: ""},
	})

	assert.NotEmpty(buf, "Expected ChecforSpamIssue to write event to buf")
}
