package spamcheck

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"math"

	"github.com/abadojack/whatlanggo"
	"gitlab.alpinelinux.org/alpine/infra/gitlab-antispam/critic"
	"golang.org/x/exp/slog"
)

type SpamCheckServiceOptions struct {
	ClassificationThreshold int
	ScoreThreshold          float32
	EventWriter             io.Writer
}

type SpamCheckService struct {
	*UnimplementedHealthServer

	classifier      *critic.Classifier
	options         SpamCheckServiceOptions
	jsonEventWriter *json.Encoder
}

func NewSpamCheckService(c *critic.Classifier, o *SpamCheckServiceOptions) SpamCheckService {
	s := SpamCheckService{
		classifier: c,
	}

	if o != nil {
		s.options = *o
	}

	if s.options.ScoreThreshold == 0 {
		s.options.ScoreThreshold = 0.5
	}

	if s.options.ClassificationThreshold == 0 {
		s.options.ClassificationThreshold = 20
	}

	if s.options.EventWriter != nil {
		s.jsonEventWriter = json.NewEncoder(s.options.EventWriter)
	}

	return s
}

var _ SpamcheckServiceServer = (*SpamCheckService)(nil)

func logistic(x int, offset int) float32 {
	return float32(1 / (1 + (math.Pow(math.E, float64(-(x - offset))))))
}

func (s SpamCheckService) CheckForSpamGeneric(ctx context.Context, generic *Generic) (*SpamVerdict, error) {
	slog.Debug("received generic", "type", generic.Type, "user", generic.User)
	return &SpamVerdict{
		Verdict:   SpamVerdict_ALLOW,
		Score:     0,
		Evaluated: false,
	}, nil
}

func (s SpamCheckService) CheckForSpamIssue(ctx context.Context, issue *Issue) (*SpamVerdict, error) {
	if err := s.writeEvent(issue); err != nil {
		slog.Warn("could not write event for issue", "error", err)
	}
	script := "unknown"
	langInfo := whatlanggo.Detect(issue.Title + "\n" + issue.Description)
	if langInfo.Script != nil {
		script = whatlanggo.Scripts[langInfo.Script]
	}

	classifications, err := s.classifier.Classify(critic.Classified{
		User: critic.User{
			Username: issue.User.Username,
			ID:       int(issue.User.Id),
		},
		Item: critic.Item{
			Title:       issue.Title,
			Description: issue.Description,
			Locale: critic.Locale{
				Language:   langInfo.Lang.Iso6393(),
				Script:     script,
				Confidence: max(0, langInfo.Confidence),
			},
		},
	})
	if err != nil {
		return nil, err
	}

	for _, classification := range classifications {
		slog.Debug("classification", "score", classification.Score, "description", classification.Description)
	}

	score := logistic(classifications.Score(), s.options.ClassificationThreshold)
	verdict := SpamVerdict_ALLOW
	if score >= s.options.ScoreThreshold {
		verdict = SpamVerdict_DISALLOW
	}

	slog.Info(
		"received issue",
		"project", issue.Project.ProjectPath,
		"title", issue.Title,
		"user", issue.User.Username,
		"score", fmt.Sprintf("%0.3f", score),
		"verdict", verdict.String(),
		"classification", classifications.Score(),
		"language", langInfo.Lang.Iso6393(),
		"script", script,
		"confidence", langInfo.Confidence,
		"action", issue.Action.String(),
	)

	return &SpamVerdict{
		Verdict:   verdict,
		Score:     score,
		Evaluated: false,
	}, nil
}

func (s SpamCheckService) CheckForSpamSnippet(ctx context.Context, snippet *Snippet) (*SpamVerdict, error) {
	if err := s.writeEvent(snippet); err != nil {
		slog.Warn("could not write event for snippet", "error", err)
	}
	if snippet == nil {
		slog.Info("Received nil snippet")
		return nil, nil
	}
	script := "unknown"
	langInfo := whatlanggo.Detect(snippet.Title + "\n" + snippet.Description)
	if langInfo.Script != nil {
		script = whatlanggo.Scripts[langInfo.Script]
	}
	classifications, err := s.classifier.Classify(critic.Classified{
		User: critic.User{
			Username: snippet.User.Username,
			ID:       int(snippet.User.Id),
		},
		Item: critic.Item{
			Title:       snippet.Title,
			Description: snippet.Description,
			Locale: critic.Locale{
				Language:   langInfo.Lang.Iso6393(),
				Script:     script,
				Confidence: max(0, langInfo.Confidence),
			},
		},
	})
	if err != nil {
		return nil, err
	}
	for _, classification := range classifications {
		slog.Debug("classification", "score", classification.Score, "description", classification.Description)
	}

	score := logistic(classifications.Score(), s.options.ClassificationThreshold)
	verdict := SpamVerdict_ALLOW
	if score >= s.options.ScoreThreshold {
		verdict = SpamVerdict_DISALLOW
	}
	slog.Info(
		"received snippet",
		"title", snippet.Title,
		"user", snippet.User.Username,
		"score", fmt.Sprintf("%0.3f", score),
		"verdict", verdict.String(),
		"classification", classifications.Score(),
		"language", langInfo.Lang.Iso6393(),
		"script", script,
		"confidence", langInfo.Confidence,
		"action", snippet.Action.String(),
	)

	return &SpamVerdict{
		Verdict:   verdict,
		Score:     score,
		Evaluated: false,
	}, nil
}

func (s SpamCheckService) writeEvent(event any) error {
	if s.jsonEventWriter != nil {
		return s.jsonEventWriter.Encode(event)
	}
	return nil
}

func (s SpamCheckService) mustEmbedUnimplementedSpamcheckServiceServer() {
}
