package spreader

import (
	"errors"
	"sync"
)

type PeriodicScheduler[T any] struct {
	store       []*T
	step        int
	insertIndex int
	getIndex    int
	mu          sync.Mutex
}

var TooManyElementsError = errors.New("too many elements")
var IncompatibleStepError = errors.New("invalid step value")

func New[T any](period, step int) (*PeriodicScheduler[T], error) {
	s := &PeriodicScheduler[T]{
		step: step,
	}

	err := s.resize(period)
	return s, err
}

func (s *PeriodicScheduler[T]) SetPeriod(newPeriod, step int) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	if step != 0 {
		s.step = step
	}
	return s.resize(newPeriod)
}

func (s *PeriodicScheduler[T]) resize(newSize int) error {
	if s.store == nil {
		s.store = make([]*T, newSize)
		if s.step == 0 {
			s.step = 29
		}

		if len(s.store)%s.step == 0 {
			return IncompatibleStepError
		}
		return nil
	}

	if newSize%s.step == 0 {
		return IncompatibleStepError
	}

	newStore := make([]*T, newSize)

	i := 0
	for _, elem := range s.store {
		if elem != nil {
			newStore[i] = elem
			i = (i + s.step) % newSize
		}
	}

	s.store = newStore
	s.getIndex = 0
	s.insertIndex = i

	return nil
}

func (s *PeriodicScheduler[T]) Add(elem *T) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	if s.store[s.insertIndex] != nil {
		return TooManyElementsError
	}

	s.store[s.insertIndex] = elem
	s.insertIndex = (s.insertIndex + s.step) % len(s.store)

	return nil
}

func (s *PeriodicScheduler[T]) Next() *T {
	s.mu.Lock()
	defer s.mu.Unlock()

	elem := s.store[s.getIndex]
	s.getIndex = (s.getIndex + 1) % len(s.store)

	return elem
}

func (s *PeriodicScheduler[T]) Remove(elem *T) bool {
	for i, e := range s.store {
		if e == elem {
			s.mu.Lock()
			s.store[i] = nil
			s.resize(len(s.store))
			s.mu.Unlock()
			return true
		}
	}

	return false
}
