package spreader

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestSchedulerInsertsItemsInExpectedOrder(t *testing.T) {
	type Elem struct {
		v int
	}

	s, err := New[Elem](5, 3)
	require.NoError(t, err, "expected no errors with period 5 and step 3")

	s.Add(&Elem{1})
	s.Add(&Elem{2})
	s.Add(&Elem{3})
	s.Add(&Elem{4})
	s.Add(&Elem{5})

	assert.Equal(t, 1, s.Next().v)
	assert.Equal(t, 3, s.Next().v)
	assert.Equal(t, 5, s.Next().v)
	assert.Equal(t, 2, s.Next().v)
	assert.Equal(t, 4, s.Next().v)
}

func TestSchedulerResizesProperly(t *testing.T) {
	type Elem struct {
		v int
	}

	s, err := New[Elem](5, 3)
	require.NoError(t, err, "expected no errors with period 5 and step 3")

	s.Add(&Elem{1})
	s.Add(&Elem{2})
	s.Add(&Elem{3})
	s.Add(&Elem{4})
	s.Add(&Elem{5})

	s.resize(10)

	assert.Equal(t, 1, s.Next().v)
	assert.Nil(t, s.Next())
	assert.Equal(t, 4, s.Next().v)
	assert.Equal(t, 3, s.Next().v)
	assert.Nil(t, s.Next())
	assert.Nil(t, s.Next())
	assert.Equal(t, 5, s.Next().v)
	assert.Nil(t, s.Next())
	assert.Nil(t, s.Next())
	assert.Equal(t, 2, s.Next().v)
}

func TestSchedulerReturnsErrorWhenInsertingMoreThanCapacity(t *testing.T) {
	type Elem struct {
		v int
	}

	s, err := New[Elem](5, 3)
	require.NoError(t, err, "expected no errors with period 5 and step 3")

	s.Add(&Elem{1})
	s.Add(&Elem{2})
	s.Add(&Elem{3})
	s.Add(&Elem{4})
	s.Add(&Elem{5})

	err = s.Add(&Elem{6})
	require.Error(t, err, "adding more elements than period should return an error")
	assert.ErrorIs(t, err, TooManyElementsError)
}

func TestSchudulerCanRemoveAndAddItem(t *testing.T) {
	type Elem struct {
		v int
	}

	s, err := New[Elem](5, 3)
	require.NoError(t, err, "expected no errors with period 5 and step 3")

	elem3 := &Elem{3}

	s.Add(&Elem{1})
	s.Add(&Elem{2})
	s.Add(elem3)
	s.Add(&Elem{4})
	s.Add(&Elem{5})

	s.Remove(elem3)
	s.Add(&Elem{6})

	assert.Equal(t, 1, s.Next().v)
	assert.Equal(t, 2, s.Next().v)
	assert.Equal(t, 6, s.Next().v)
	assert.Equal(t, 5, s.Next().v)
	assert.Equal(t, 4, s.Next().v)
}

func TestSchedulerRejectsIncmpattibleSteps(t *testing.T) {
	_, err := New[int](8, 4)
	require.Error(t, err, "expect period 8 and step 4 to return an error")
	assert.ErrorIs(t, err, IncompatibleStepError)
}
